doxy-runner セットアップ           {#setup}
===========================================
[TOC]

GitLab の CI/CD での利用を想定した gitlab-runner用 イメージ/コンテナです.

# 1. doxygen コンテナ の ビルド
## 1.1. ダウンロード済みファイルの準備 
### 1) イメージをビルドする ディレクトリに移動
```
cd docker/
```

### 2) ダウンロード済ファイル ディレクトリを見てみる
```
ls -H .archive/
```

### 実行例
```
~/r/doxy-runner/docker$ ls -H .archive
doxygen-1.10.0.src.tar.gz  gitlab-runner-linux-amd64-v16.9.1  plantuml-1.2024.3.jar
$
```

### 3) ダウンロード済ファイルの入手元
- doxygen       https://github.com/doxygen/doxygen/releases
- PlantUML      https://github.com/plantuml/plantuml/releases
- gitlab-runner https://gitlab.com/gitlab-org/gitlab-runner/-/releases

## 1.2. イメージ構築用 アーカイブの作成

```
tar -cvzhf doxy-runner.tar.gz Dockerfile entrypoint .archive*
```

### 実行例
```
mrymakr@r731:~/r/doxy-runner/docker$ tar -cvzhf doxy-runner.tar.gz Dockerfile entrypoint .archive*
Dockerfile
entrypoint
.archive/
.archive/plantuml-1.2024.3.jar
.archive/doxygen-1.10.0.src.tar.gz
.archive/gitlab-runner-linux-amd64-v16.9.1
$
```

## 1.3. イメージ ビルド

```
docker build -t doxy-runner:20240319 -< doxy-runner.tar.gz
```

### 実行例
```
@x500:~/r/doxy-runner/docker$ docker build -t doxy-runner:20240319 -< doxy-runner.tar.gz
[+] Building 3.9s (23/23)                     FINISHED
```

------

# 2. doxygen コンテナ の 実行
## 2.4. コンテナの実行とCI/CD登録

### 2.4.1. 社内 gitlab サーバー
社内 gitlab サーバー への登録では コンテナ名を 対象プロジェクト毎に起動する必要があります.

#### 1) 登録トークン情報の 取得
![](image/RegisterToken.png)

#### 2) コンテナ の実行
コンテナ名の 末尾に ```runner``` を付与.
```
docker run -d --restart=always -v doxygen:/home/doxygen --name doxy-20240319-runner doxy-runner:20240319
```

```
~/r/doxy-runner/docker$ docker run -d --restart=always -v doxygen:/home/doxygen --name doxy-20240319-runner doxy-runner:20240319
9d0ef448625dc4596f4159e3ea53e872889b4036a0c4f58b67e1a5efa22d4164
$
```

#### 3) gitlab-runner の 登録
##### コマンド
```
docker exec -it doxy-20240319-runner gitlab-runner register --tag-list linux,docker,doxygen --executor shell
```

コンテナ内で gitlab-runner register が実行されるので 必要な情報を コンソールから入力する.
```
~/r/doxy-runner/docker$ docker exec -it doxy-20240319-runner gitlab-runner register --tag-list linux,docker,doxygen --executor shell
gitlab-runner register --tag-list linux,docker,doxygen --executor shell
Runtime platform                                    arch=amd64 os=linux pid=108 revision=102c81ba version=16.9.1
WARNING: Running in user-mode.
WARNING: The user-mode requires you to manually start builds processing:
WARNING: $ gitlab-runner run
WARNING: Use sudo for system-mode:
WARNING: $ sudo gitlab-runner...
```

##### URL
```
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://bizsoftdevelopers.ebz.epson.net/git1/
```
前記 web ページより ```URL``` 情報を コピペする

##### 登録トークン
```
Enter the registration token:
3GjLm9sbhAANLxQbmz2h
```
前記 web ページより ```登録トークン``` 情報を コピペする

##### runner の説明
```
Enter a description for the runner:
[9d0ef448625d]: 9d0ef448625d jp2200326
```
コンテナID と ホスト名を 入力する

##### タグ
```
Enter tags for the runner (comma-separated):
[linux,docker,doxygen]:
```
そのまま ```linux,docker,doxygen``` でENTER

##### 説明
```
Enter optional maintenance note for the runner:

WARNING: Support for registration tokens and runner parameters in the 'register' command has been deprecated in GitLab Runner 15.6 and will be replaced with support for authentication tokens. For more information, see https://docs.gitlab.com/ee/ci/runners/new_creation_workflow
Registering runner... succeeded                     runner=3GjLm9sb
```
そのまま 未入力で ENTER

##### エクゼキュタ
```
Enter an executor: docker, docker-windows, shell, virtualbox, custom, ssh, docker-autoscaler, docker+machine, instance, kubernetes, parallels:
[shell]:
```
そのまま ```shell``` でENTER

```
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Configuration (with the authentication token) was saved in "/home/gitlab-runner/.gitlab-runner/config.toml"
$
```

以上で 設定完了

---------------------------
### 2.4.2. GitLab.com 
GitLab.com  への登録では コンテナを 共有利用することが出来るため １つのコンテナを登録するだけで 利用可能です.

#### 1) コンテナ の実行
```
docker run -d --restart=always -v doxygen:/home/doxygen --name doxy-20240319 doxy-runner:20240319
```

```
~/r/doxy-runner/docker$ docker run -d --restart=always -v doxygen:/home/doxygen --name doxy-20240319 doxy-runner:20240319
5b1f92af6a140cbcc8672b0ba1be7e81f1f6161f460d1955bfa04a59acf895c6
$
```

#### 2) 新規プロジェクトRunner の作成
![](image/cicd1.png)

[新規プロジェクトRunner]ボタンをクリック

##### 新規プロジェクトRunner 画面の設定
![](image/cicd2.png)

##### プラットフォーム
プラットフォームは ```オペレーティングシステム``` - ```Linux``` を 選択

##### Tags
Tags は ```docker,linux,doxygen``` を入力

##### 詳細 (オプション) - Runnerの説明
Runnerの説明 は ホスト名とコンテナID を入力
```
x500 5b1f92af6a140cbcc8672b0ba1be7e81f1f6161f460d1955bfa04a59acf895c6
```

##### 設定 (オプション) - ジョブタイムアウトの最大値
```
600
```

##### [ランナーを作成] ボタンをクリックする

#### 3) GitLab.com との連携
![](image/cicd3.png)

##### コマンド
```
docker exec -it doxy-20240101 \
```
##### コマンド(続き) コピペ
```
> gitlab-runner register  --url https://gitlab.com  --token glrt-QxqLkWFsNaj4iRXEDe4Q
```

##### 実行例
~~~
~/r/doxy-runner/docker$ docker exec -it doxy-20240319 \
> gitlab-runner register  --url https://gitlab.com  --token glrt-QxqLkWFsNaj4iRXEDe4Q
~~~
~~~
Runtime platform                                    arch=amd64 os=linux pid=75 revision=102c81ba version=16.9.1
WARNING: Running in user-mode.
WARNING: The user-mode requires you to manually start builds processing:
WARNING: $ gitlab-runner run
WARNING: Use sudo for system-mode:
WARNING: $ sudo gitlab-runner...
~~~
##### URL
```
Enter the GitLab instance URL (for example, https://gitlab.com/):
[https://gitlab.com]:
Verifying runner... is valid                        runner=QxqLkWFsN
```

##### ランナー名
```
Enter a name for the runner. This is stored only in the local config.toml file:
[5b1f92af6a14]:
```

##### エクゼキュタ
```
Enter an executor: custom, shell, docker+machine, instance, docker, docker-windows, parallels, ssh, virtualbox, docker-autoscaler, kubernetes:
shell
```
```
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
Configuration (with the authentication token) was saved in "/home/gitlab-runner/.gitlab-runner/config.toml"
$
```

以上で 設定完了

----

# 3. PlantUML 例

```plantuml
@startuml projects
Bob -> Alice : hello1
@enduml
```

// !include hello.pu

----

# 4. doxygen の markdown サポート

https://doxygen.knowhow.jp/markdown.html

https://www.doxygen.nl/manual/markdown.html

# 5. QuickStart
コピペ用
~~~sh
 # doxy-gitlab-runner イメージの作成
docker compose build --no-cache # apt-get の キャッシュも更新する場合
 # compose up を使った 社内GitLabへの runner登録
docker compose up -d --build
docker ps -a\
 --format "table {{.ID}}\t{{.Names}}\t{{.State}}\t{{.Status}}\t{{.Image}}"\
 --filter name=doxy
docker inspect doxy-gitlab-runner-1 |jq .[].HostConfig.RestartPolicy
docker exec -it doxy-gitlab-runner-1 gitlab-runner register\
 --tag-list linux,doxygen --executor shell

 # pf_compose ドキュメント runner登録
docker compose run -d -v htmlDocs:/mnt/htmlDocs --name doxy-gitlab-pf_compose runner
docker update --restart=always doxy-gitlab-pf_compose
docker exec -it doxy-gitlab-pf_compose gitlab-runner register\
 --tag-list linux,doxygen --executor shell

 # various_viewer ドキュメント runner登録
docker compose run -d -v htmlDocs:/mnt/htmlDocs --name doxy-gitlab-various_viewer runner
docker update --restart=always doxy-gitlab-various_viewer
docker exec -it doxy-gitlab-various_viewer gitlab-runner register\
 --tag-list linux,doxygen --executor shell

 # insp_app ドキュメント runner登録
docker compose run -d -v htmlDocs:/mnt/htmlDocs --name doxy-gitlab-insp_app runner
docker update --restart=always doxy-gitlab-insp_app
docker exec -it doxy-gitlab-insp_app gitlab-runner register\
 --tag-list linux,doxygen --executor shell

 # vis_con ドキュメント runner登録
docker compose run -d -v htmlDocs:/mnt/htmlDocs --name doxy-gitlab-vis_con runner
docker update --restart=always doxy-gitlab-vis_con
docker exec -it doxy-gitlab-vis_con gitlab-runner register\
 --tag-list linux,doxygen --executor shell

 # 生成ドキュメントのアーカイブ
docker cp doxy-gitlab-runner-1:/mnt/htmlDocs - | tar tvf - 
docker cp doxy-gitlab-runner-1:/mnt/htmlDocs - | gzip -9 > doxy-runner-doxygen.tar.gz

 # compose up を使った gitlab.comへの runner登録
docker compose build --no-cache
docker compose up -d --build
docker exec -it doxy-gitlab-runner-1 \
 <ペースト>
~~~
